package fekProject;

import au.com.bytecode.opencsv.CSVReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * @author A. Tzanis
 */


public class CompanySearch {
	
	/** 
	 *  Search for FEK files related to a company vat ID
	 */
    @SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception,IOException {

    	HelperMethods hm = new HelperMethods();
    	
    	String inputFileName = "Queries.csv";				//Name of the input file
    	int delayTime = 5;									//delayTime variable
    	String companyVatId = null; 						//Company vat id
    	int counter = 0;									//Counts the number of searches made
    	    	
    	SearchByCompanyType search = new SearchByCompanyType();
		
		try { 
	   		
			@SuppressWarnings("resource")
			CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(inputFileName), "UTF-8"), 
					 ',', '"', '\0');
			
			String[] nextLine;

			while ((nextLine = reader.readNext()) != null) {					// Reads the csv file by row 
				
				companyVatId = nextLine[0];	
				
				if (counter != 0 && counter%50 == 0){									//�dd time delay every 50 repetitions
					System.out.println(delayTime+ " seconds pause began!!!");
					TimeUnit.SECONDS.sleep(delayTime);
					System.out.println("The search began again!!!");
				}
	   		
				if (companyVatId != null){												//Checks if there are more vatId to search for
					
					try {
						
						hm.writeUnknownMetadata("processedVatId", companyVatId);
						boolean exists = hm.fileChecker(companyVatId);						// Check if file already exists
							
						if (exists == false){
							System.out.println("��� ��������� ���� ��������� : " +companyVatId);
							search.Search(companyVatId);								// if file does not exist call search
						}
							
						else {
							continue;													// else if file exist continue to next one
						}
							
					} 
					catch (Exception e) {
					e.printStackTrace();
					}
					counter++;															//Increase the counter by one
				}
			}
		}
		catch ( IOException ex ) { 
    	ex.printStackTrace(); 
		}
    }
 }