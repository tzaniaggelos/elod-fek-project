package fekProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * 
 * @author Aggelos
 *
 */

public class HelperMethods {

	/** 
     * Check if a file exists in a directory 
     */ 
    public static boolean fileChecker(String vatId){
    	
    	boolean flag = false;
       
    	String aeDirectoryPath = "/home/tzanis/theFekProject/fekHarvesting/AE/AE/"; 					// AE directory path in server
    	String epeDirectoryPath = "/home/tzanis/theFekProject/fekHarvesting/EPE/"; 					// EPE directory path in server
    	
    	//String aeDirectoryPath = "C:/Users/Aggelos/workspace/TheFekProject/AEserver/"; 
    	//String epeDirectoryPath = "C:/Users/Aggelos/workspace/TheFekProject/EPE/";
    	
        File aeCsv = new File(aeDirectoryPath + vatId + ".csv");				// Path of the ae csv file if it already exists
        File epeCsv = new File(epeDirectoryPath + vatId + ".csv");				// Path of the epe csv file if it already exists
    	
        // check if file exists in AE directory
        if (aeCsv.exists()){
			System.out.println("\nFile " + vatId + ".csv already exist in AE directory!\n");
			writeUnknownMetadata("AeVatIdList", vatId);
			flag = true;
		}
        // check if file exists in EPE directory
        else if (epeCsv.exists()){
			System.out.println("\nFile " + vatId + ".cdv already exist in EPE directory!\n");
			writeUnknownMetadata("EpeVatIdList", vatId);
			flag = true;
		}
        return flag;						// returns true if file exists and false if not
    }
    
    
    /**
     * Export to a file the unknown metadata.
     * 
     * @param String the output filename
     * @param String the unknown metadata
     */
	public static void writeUnknownMetadata(String fileName, String metadata) {
		
		try {
		    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName + ".csv", true), "UTF8"));
		    out.append(metadata);
		    out.append(System.getProperty("line.separator"));
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
    
	
}
