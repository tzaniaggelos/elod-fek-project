package fekProject;

import java.io.BufferedWriter;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * @author A. Tzanis
 */

public class RelatedFiles {
	
	@SuppressWarnings("unused")
	public RelatedFiles(String link , BufferedWriter bufferedWriter, String afm) throws IOException, InterruptedException {
		
		String scrappingDate ;							//Current date
		String subject = null;							//Subject of the FEK
		String fekNumber = null;						//Number of the FEK
		String fekType = null;							//Type of FEK
		String origDate = null;							//Original release date of the FEK
		String publDate = null;							//Publication date of the FEK
		String pdfLink = null;							//PDF download link url
		String companyFullName = null;					//Company full name
		String fekDetails = null;						//FEK details
		String testPath = null;
		String fekStatus = null;						//Status of FEK, publicated or in progress 
		int bCount = 0;									//Variable to store the count of <b>
		
		java.sql.Date dt2 = new java.sql.Date(System.currentTimeMillis());
		scrappingDate = dt2.toString();
		//scrappingDate=scrappingDate.substring(8, 10)+"."+scrappingDate.substring(5,7)+"."+scrappingDate.substring(0, 4);
		
		//System.out.println("--------------- ������������ �� �.�.�. ---------------");
		
		WebDriver driver = new HtmlUnitDriver();												// Create a new instance of the html unit driver

        driver.get(link);																		// And now driver visit the results page
        
        int rowCount = driver.findElements(By.xpath("//table[2]/tbody/tr")).size()-1;			// Finds the count of relates to the company FEK
        System.out.println("������� �������� �������:" +rowCount);
        
        // Print informations for each company
        for (int i=0; i<rowCount; i++){
        	
        	int num = 2+i;
        	fekStatus = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[1]")).getText().substring(0,22);		//Variable to store the status of the FEK
        	bCount = driver.findElements(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b")).size();								//Varieble for checking if there is a publication date or not
        	companyFullName = driver.findElement(By.xpath("//table[2]/tbody/tr[1]/td/b/span")).getText();							//Variable to store the full company name
        	fekDetails = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]")).getText();							//Variable to store all the informations of the related FEK
    	   
        	// We take only the fek that has been publicated
        	if (!fekStatus.equalsIgnoreCase("�� ������ ������������")){
        		
        		//Check if div[2] contains the subject of the FEK or the id of the sender
        		testPath = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/div[2]")).getText().substring(0 , 4);  
    	   
        		// case div[2] contains the subject
        		if (testPath.equalsIgnoreCase("����")){
        			
        			subject = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/div[2]")).getText().substring(6);		//Variable to store the subject of the FEK
        			fekNumber = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[1]")).getText().substring(28);		//Variable to store the number of the FEK
        			fekType = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[1]")).getText().substring(21,27);		//Variable to store the type of the FEK
        			origDate = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[2]")).getText().substring(2);		//Variable to store the original release date of the FEK
        			origDate = origDate.substring(6, 10)+"-"+origDate.substring(3,5)+"-"+origDate.substring(0, 2);
        			
        			// case there is a publication date
        			if (bCount == 3){   
        				publDate = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[3]")).getText().substring(5);	//Variable to store the publication date of the FEK if it exist
        				publDate = publDate.substring(6, 10)+"-"+publDate.substring(3,5)+"-"+publDate.substring(0, 2);
            		}
    		   
        			WebElement element = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[3]/span[2]/a"));				// Find the pdf download button element
        			pdfLink = element.getAttribute("href").toString();																	// Stores the url of the download link
        		}
    	   
        		// case div[3] contains the subject
        		else if (testPath.equalsIgnoreCase("����")){
    		   
        			subject = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/div[3]")).getText().substring(6);		//Variable to store the subject of the FEK
        			fekNumber = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[1]")).getText().substring(28);		//Variable to store the type of the FEK
        			fekType = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[1]")).getText().substring(21,27);		//Variable to store the type of the FEK
        			origDate = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[2]")).getText().substring(2);		//Variable to store the original release date of the FEK
        			origDate = origDate.substring(6, 10)+"-"+origDate.substring(3,5)+"-"+origDate.substring(0, 2);
        			
        			// case there is a publication date
        			if (bCount == 3){   
        				publDate = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[3]")).getText().substring(5);	//Variable to store the publication date of the FEK if it exist
        				publDate = publDate.substring(6, 10)+"-"+publDate.substring(3,5)+"-"+publDate.substring(0, 2);					//Convert publDate format from 26.11.2015 to 2015-11-26
            		}
        	   
        			WebElement element = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[3]/span[2]/a"));				// Find the pdf download button element
        			pdfLink = element.getAttribute("href").toString();																	// Stores the url of the download link
        		}
    	   	
    	   
        			//System.out.println("--------------- " +(i+1)+ " ---------------");
        			//System.out.println(fekDetails + "\n" +pdfLink+ "\n");
    	   
        			// Check if fekNumber starts with . (.T. ) and not with the fek number
        			if (fekNumber.substring(0,1).equalsIgnoreCase(".")){
        				fekNumber = fekNumber.substring(4);
        			}
        			
        			// Check if fekType is "���.�.�.�." or "��-���"
        			if (fekType.equals("���.�.")){
        				fekType = driver.findElement(By.xpath("//table[2]/tbody/tr[" +num+ "]/td[2]/b[1]")).getText().substring(21,31);
        			}
        			
        			bufferedWriter.write(fekNumber+ "," +fekType+ "," +origDate+ "," +publDate+ "," +subject+ "," +pdfLink+ "," +scrappingDate);		//Writes the results to the file
        			bufferedWriter.newLine();
        			
        	}
	   	   		publDate = null;						// Set publDate to null in case we have not a publication date information
	   	   		fekNumber = null;
	   	   		fekType = null;
    	  }
        
        driver.quit();									//Finally close the driver
        
	}

}