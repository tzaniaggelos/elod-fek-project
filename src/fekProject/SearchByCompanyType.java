package fekProject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

import fekProject.HelperMethods;

public class SearchByCompanyType {

	@SuppressWarnings({ "unused", "static-access" })
	public void Search(String vatId) throws IOException, InterruptedException{
		
		HelperMethods hm = new HelperMethods();
		
		String scrappingDate; 								//Current date
    	String link = null;									//Results page link
    	String companyFullName = null;						//Full company name
		String companyTitle = null;							//Company title
		String companyMae = null;							//Company MAE id
		String companyGemi = null;							//Company GEMI id
		String filePath = null;								//Output filepath
		int results = 0;									//Check if et.gr has results for the given vatId
		BufferedWriter bufferedWriter = null;
		int k = 1;
		boolean found = false;
		String compType = null;
		
		java.sql.Date dt1 = new java.sql.Date(System.currentTimeMillis());		//Get the current date in format 26-11-2015
		scrappingDate = dt1.toString();
		
		WebDriver driver = new HtmlUnitDriver();								// Create a new instance of the HTML unit driver

		while (k<3 && found == false){
			
			if (k==1) {compType="1";}
			else if (k==2){compType="2";}
		
			driver.get("http://www.et.gr/idocs-nph/search/fekAeEpeForm.html");		// And now driver visit www.et.gr

			Select companyType = new Select(driver.findElement(By.id("companyType")));
		
			companyType.selectByValue(compType);
		
			driver.findElement(By.cssSelector("input[value='afm']")).click();		//Find and click the AFM radio button
		
			WebElement element = driver.findElement(By.name("afm"));				// Find the text input element of AFM

			element.sendKeys(vatId);												// Enter the AFM we want to search
			element.submit();														// Now submit the form

			// Check if site has results for the company
			results = driver.findElements(By.id("link")).size();
		
			//If site has results for A.E.
			if (results>0 && compType.equalsIgnoreCase("1")){
				
				found = true;
			
				link = driver.findElement(By.id("link")).getAttribute("onclick").toString();		// Takes the onclick button link url

				link = "http://www.et.gr"+link.substring(18 , link.length()-2);						// Converts the link url, to a correct link url structure 

				// Find using xpath the information we want. expath is found with the help of firebug.
				companyFullName = driver.findElement(By.id("link")).getText();										//Variable to store the full company name
				companyTitle = driver.findElement(By.xpath("//table[2]/tbody/tr[1]/td/div[1]")).getText();			//Variable to store the company title
				companyMae = driver.findElement(By.xpath("//table[2]/tbody/tr[1]/td/div[2]")).getText();			//Variable to store the company MAE id
				companyGemi = driver.findElement(By.xpath("//table[2]/tbody/tr[1]/td/div[3]")).getText();			//Variable to store the company GEMI id
				//System.out.println("--------------- �������� ��������� AE ---------------");
				//System.out.print(companyFullName+ "\n" +companyTitle+ "\n" +companyMae+ "\n" +companyGemi+ "\n");

				//filePath = "C:/Users/Aggelos/workspace/TheFekProject/" +vatId+ ".csv";		//Variable to store the output filepath
				//filePath = "C:/Users/Aggelos/workspace/TheFekProject/AE/" +vatId+ ".csv";		//Variable to store the output filepath
				filePath = "/home/tzanis/theFekProject/fekHarvesting/AE/AE/"+ vatId +".csv";		//Variable to store the output filepath
			
				driver.quit();
			
				try {

					bufferedWriter = new BufferedWriter(new FileWriter(filePath));										//Creation of bufferedWriter object

					bufferedWriter.write(vatId+ "," +companyMae.substring(16)+ "," +companyGemi.substring(14,26)+ "," +companyTitle.substring(20).replace(',','.')+","+link+ "," +scrappingDate);					//Writes the results to the file
					bufferedWriter.newLine();
		
					hm.writeUnknownMetadata("AeVatIdList", vatId);
					
					try {
						new RelatedFiles(link , bufferedWriter, vatId); 														//Call RelatedFiles class
					} catch (IOException e) {
						e.printStackTrace();
					}

				}

				finally{													//Close the BufferedWriter

					try {				
						if (bufferedWriter != null) {
							bufferedWriter.flush();
							bufferedWriter.close();
						}
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				break;
			}
		
			//If site has results for �.�.�.
			else if (results>0 && compType.equalsIgnoreCase("2")){
				
				found = true;
			
				link = driver.findElement(By.id("link")).getAttribute("onclick").toString();		// Takes the onclick button link url

				link = "http://www.et.gr"+link.substring(18 , link.length()-2);						// Converts the link url, to a correct link url structure 

				// Find using xpath the information we want. expath is found with the help of firebug.
				companyFullName = driver.findElement(By.id("link")).getText();										//Variable to store the full company name
				companyTitle = driver.findElement(By.xpath("//table[2]/tbody/tr[1]/td/div[1]")).getText();			//Variable to store the company title
				companyGemi = driver.findElement(By.xpath("//table[2]/tbody/tr[1]/td/div[2]")).getText();			//Variable to store the company GEMI id
				//System.out.println("--------------- �������� ��������� ��� ---------------");
				//System.out.print(companyFullName+ "\n" +companyTitle+ "\n" +companyGemi+ "\n");

				//filePath = "C:/Users/Aggelos/workspace/TheFekProject/" +vatId+ ".csv";		//Variable to store the output filepath
				//filePath = "C:/Users/Aggelos/workspace/TheFekProject/EPE/" +vatId+ ".csv";		//Variable to store the output filepath
				filePath = "/home/tzanis/theFekProject/fekHarvesting/EPE/"+ vatId +".csv";		//Variable to store the output filepath
			
				driver.quit();
			
				try {

					bufferedWriter = new BufferedWriter(new FileWriter(filePath));										//Creation of bufferedWriter object

					bufferedWriter.write(vatId+ "," +companyGemi.substring(14,26)+ "," +companyTitle.substring(20).replace(',','.')+","+link+ "," +scrappingDate);					//Writes the results to the file
					bufferedWriter.newLine();
				
					hm.writeUnknownMetadata("EpeVatIdList", vatId);
					
					try {
						new RelatedFiles(link , bufferedWriter, vatId); 														//Call RelatedFiles class
					} catch (IOException e) {
						e.printStackTrace();
					}

				}

				finally{													//Close the BufferedWriter

					try {				
						if (bufferedWriter != null) {
							bufferedWriter.flush();
							bufferedWriter.close();
						}
						} catch (IOException ex) {
							ex.printStackTrace();
						}
				}
				break;
			}
		
			k++;														// change the company type --> 1=AE, 2=EPE
		}
		//Case there are not results for the company
		if (results==0){
		//	System.out.println("��� �������� ������������ ��� �� ���: "+vatId);
			hm.writeUnknownMetadata("UnmatchedVatIdList", vatId);
		}
		
	}
}